class TalksController < ApplicationController
  before_action :set_talk, only: [:show, :edit, :update, :destroy]
  def index
    @talks = Talk.all
  end

  def new
    @talk = Talk.new
    @talk.attendees.build
  end

  def show
  end

  def edit
  end

  def create
    @talk = Talk.new talks_params
    @talk.user_id = current_user.id
    binding.pry
    respond_to do |format|    
      if @talk.save
        format.html { redirect_to talks_path, notice: 'Talks created Succesfully.' }
      else
        format.html { render :new }
      end
    end
  end


  def update
    respond_to do |format|
      if @talk.update(talks_params)
        format.html { redirect_to talks_path, notice: 'Talks Updated Succesfully.' }
      else
        format.html { render :edit }
      end
    end    
  end

  def destroy
      binding.pry
      respond_to do |format|
      if params[:attendee_id]
        @talk.attendees.destroy( params[:attendee_id])
         format.html { redirect_to talk_path(params[:id]), notice: 'Attendee was successfully destroyed.' }
      else
        @talk.destroy
         format.html { redirect_to talks_path, notice: 'Talks was successfully destroyed.' }
      end
    end    
  end

  private

  def set_talk
    @talk = Talk.find(params[:id])
  end
  def talks_params
    params.require(:talk).permit(:title, :abstract, :room,  attendees_attributes: [:id,:name, :company, :email, :registered])
  end
end
