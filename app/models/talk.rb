class Talk < ApplicationRecord
  belongs_to :user
  has_many :attendees, dependent: :destroy
  accepts_nested_attributes_for :attendees, :allow_destroy => true,:reject_if => :all_blank  
end
