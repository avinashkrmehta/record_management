Rails.application.routes.draw do
  devise_for :users
  resources :talks

  get 'home/dashboard'
	devise_scope :user do
	  authenticated :user do
	     get '/users/sign_out' => 'devise/sessions#destroy'
	    root  'home#dashboard', as: :authenticated_root
	  end
	  unauthenticated do
	    root 'devise/sessions#new', as: :unauthenticated_root
	  end
	end  
end
