class CreateAttendees < ActiveRecord::Migration[5.2]
  def change
    create_table :attendees do |t|
      t.string :name
      t.string :company
      t.string :email
      t.datetime :registered
      t.references :talk, foreign_key: true

      t.timestamps
    end
  end
end
